(function ($) {
Drupal.behaviors.gridster = {
  attach:
    function(context) {

      var widget_base_x = 240;
      var widget_base_y = 20;

      // Calculate a height for each of the blocks.
      $(".gridster > li").each(function (i) {
        var height = $(this).height();
        var units = Math.ceil(height / widget_base_y);

        $(this).attr('data-sizey', units);
      });

      // Enable Gridster.
      var gridster = $(".gridster").gridster({
        widget_margins: [10, 10],
        widget_base_dimensions: [widget_base_x, widget_base_y],
        serialize_params: function($w, wgd) {
          return {
              id: wgd.el[0].id,
              col: wgd.col,
              row: wgd.row,
              size_y: wgd.size_y,
              size_x: wgd.size_x
          }
        }
      }).data('gridster');

    }
}
})(jQuery);